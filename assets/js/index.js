$(document).ready(function () {
	$('.carros').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 1500,
		arrows: false,
		dots: false,
		pauseOnHover: false,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 6
			}
    }, {
			breakpoint: 520,
			settings: {
				slidesToShow: 3
			}
    }]
	});
});
$(function () {
	$('a[href*="#top"]:not([href="#"])').click(function () {
		var target = $(this.hash);
		if (target.length) {
			$("html, body").animate(
				{
					scrollTop: target.offset().top
				},
				1500
			);
		}
	});

	$('a[href*="#contato"]:not([href="#"])').click(function() {
    var target = $(this.hash);
    if (target.length) {
      $("html, body").animate({ scrollTop: target.offset().top }, 1500);
    }
  });
});
	