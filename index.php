<?php require_once "lib/Url.php"; ?>
	<!doctype html>
	<html lang="pt-br">

	<head>
		<title>America Ford</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="Ford, Carro, Manaus, Vendas, Ford Ka, Ford Ranger, Ford Eco">
		<meta name="description" content="Website do America Ford">
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://americaford.digital">
		<meta property="og:site_name" content="America Ford">
		<meta property="og:image:type" content="image/png">
		<meta property="og:description" content="Website do America Ford">
		<link rel="icon" type="image/png" href="favicon.png" />
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo URL::getBase() ?>assets/css/style.css">
		<link rel="stylesheet" href="<?php echo URL::getBase() ?>assets/css/gallery.css">
	</head>

	<body id="home">
		<header id='top'>
			<?php require('block/top.php') ?>
		</header>
		
		<?php
		    $modulo = Url::getURL( 0 );
		    if( $modulo == null )
			$modulo = "home";
			if( file_exists( "modulos/" . $modulo . ".php" ) ){
			    require "modulos/" . $modulo . ".php";
			}else{
			    require "modulos/404.php";
			}
		?>
		
		<footer class="bg-dark text-white">
			<?php //require('block/footer.php') ?>
			<?php require('block/midia.php') ?>
		</footer>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src='<?php echo URL::getBase() ?>assets/js/index.js'></script>
	</body>

	</html>
	