-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Dez-2018 às 23:42
-- Versão do servidor: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nissan_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carro`
--

CREATE TABLE `carro` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `ficha` text NOT NULL,
  `itens` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `carro`
--

INSERT INTO `carro` (`id`, `nome`, `ficha`, `itens`) VALUES
(1, 'frontier', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CARROCERIA</h3>\r\n  <p>Arquitetura:Picape cabine dupla, 4 portas, 5 lugares</p>\r\n  <h3>SUSPENSÃO</h3>\r\n  <p>Suspensão dianteira:Double-wishbone - Suspensão traseira:Multilink com molas helicoidais</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>MOTOR</h3>\r\n  <p>Tipo:2.3 l 16 válvulas, Bi-Turbo Diesel eletrônico c/ intercooler e injeção direta - Cilindrada máxima:2.298 cm³ - Potência:190 cv @ 3.750 rpm - Torque:45,9 kgfm @ 2.500 rpm - Tração:4x4 com bloqueio de diferencial traseiro mecânico</p>\r\n  <h3>FREIOS</h3>\r\n  <p>Tipo:Discos ventilados dianteiros e tambores traseiros, sistema ABS de 4 canais e 4 sensores com controle eletrônico de distribuição de força (EBD)</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>TRANSMISSÃO</h3>\r\n  <p>Tipo:Câmbio automático de 7 marchas com função manual sequencial</p>\r\n  <h3>RODAS/PNEUS</h3>\r\n  <p>Rodas:Liga leve 16” de alumínio - Pneus:255/70R16 - Estepe:255/70R16 – Liga de alumínio – 16’</p> \r\n</div>', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CÂMBIO</h3>\r\n  <p>Câmbio automático de 7 marchas com função manual sequencial:Item de série</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>CONFORTO E CONVENIÊNCIA</h3>\r\n  <p>Abertura da tampa de combustível por acionamento interno:Item de série - Acabamento de couro nos bancos:Item de série -\r\n  Acendimento inteligente dos faróis:Item de série - Apoios de cabeça dianteiros e traseiros com regulagem de altura:Item\r\n  de série - Aquecimento dos bancos dianteiros:Item de série - Ar-condicionado digital Dual Zone e filtro de pólen:Item\r\n  de série - Banco do motorista com regulagem elétrica:Item de série - Banco traseiro rebatível:Item de série - Bancos\r\n  dianteiros com tecnologia Zero Gravity®:Item de série - Chave inteligente</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>APARÊNCIA</h3>\r\n  <p>Desembaçador de vidro traseiro com timer:Item de série - Estribo lateral:Item de série - Faróis de neblina com\r\n  acabamento cromado:Item de série - Faróis dianteiros diurnos com assinatura de LED (DTRL):Item de série - Gancho para\r\n  reboque (dianteiro):Item de série - Grade frontal cromada:Item de série - Maçanetas e retrovisores externos\r\n  cromados:Item de série - Para-barro rígido nas rodas dianteiras e traseiras:Item de série - Para-brisa com proteção\r\n  UV:Item de série - Para-choques dianteiros na cor do veículo:Item de série - Para-choques</p>\r\n</div>'),
(2, 'march', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CARROCERIA</h3>\r\n  <p>Arquitetura:Hatchback, 5 portas, 5 lugares, tração dianteira</p>\r\n  <h3>SUSPENSÃO</h3>\r\n  <p>Suspensão dianteira:Estrutura independente, tipo McPherson - Suspensão traseira:Estrutura com eixo de torção</p>\r\n</div>\r\n<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>MOTOR</h3>\r\n  <p>Tipo:Dianteiro, transversal, 3 cilindros em linha, 12 válvulas, acelerador eletrônico, bicombustível (gasolina e\r\n  etanol) - Cilindrada:999 cm³ - Potência:77 cv @ 6.200 rpm (etanol/gasolina) - Torque:10 Kgfm @ 4.000 rpm\r\n  (etanol/gasolina) - Sistema de injeção:Eletrônica multiponto sequencial</p>\r\n  <h3>FREIOS</h3>\r\n  <p>Dianteiros:Discos ventilados (260 mm de diâmetro) - Traseiros:Tambores (203,2 mm de diâmetro) - Rodas:Aço, 14” -\r\n  Pneus:165/70 R14 - Fornecedor:Firestone)</p>\r\n</div>\r\n<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>TRANSMISSÃO</h3>\r\n  <p>Tipo:Manual, 5 marchas, tração dianteira - 1ª:4,091 - 2ª:2,238 - 3ª:1,393 - 4ª:1,029 - 5ª:0,795 - Ré:3,546 -\r\n  Diferencial:4,500</p>\r\n  <h3>RODAS/PNEUS</h3>\r\n  <p>Dianteiros:Discos ventilados (260 mm de diâmetro) - Traseiros:Tambores (203,2 mm de diâmetro) - Rodas:Aço, 14” -\r\n  Pneus:165/70 R14 - Fornecedor:Firestone</p>\r\n</div>', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CÂMBIO</h3>\r\n  <p>Câmbio manual de 5 marchas:Item de série</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>CONFORTO E CONVENIÊNCIA</h3>\r\n  <p>Acelerador eletrônico do motor (drive by wire):Item de série - Ar-condicionado:Item de série - Ar quente:Item de série\r\n  - Banco do motorista “Comfort Seat” com regulagem de altura:Item de série - Chave com telecomando para abertura e\r\n  fechamento das portas e do porta-malas:Item de série - Computador de bordo com as funções consumo instantâneo e\r\n  relógio:Item de série - Console central com 3 porta-copos e porta-objetos:Item de série - Conta-giros e velocímetro com\r\n  acabamento prata:Item de série - Desembaçador do vidro traseiro com</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>APARÊNCIA</h3>\r\n  <p>Acabamento prata no painel central e nas saídas de ar-condicionado:Item de série - Calotas integrais:Item de série -\r\n  Detalhe da grade superior em V cromado:Item de série - Retrovisores externos rebatíveis:Item de série - Retrovisores\r\n  externos na cor da carroceria:Item de série - Rodas de aço de 14” e pneus 165/70 R14:Item de séries</p>\r\n</div>'),
(3, 'kicks', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CARROCERIA</h3>\r\n  <p>Arquitetura:SUV, 4 portas, 5 lugares</p>\r\n  <h3>SUSPENSÃO</h3>\r\n  <p>Suspensão dianteira:Independente, tipo McPherson, com barra estabilizadora - Suspensão traseira:Eixo de torção</p>\r\n</div>\r\n<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>MOTOR</h3>\r\n  <p>Tipo:1.6 l, 16 válvulas, CVVTCS*, bicombustível, 4 cilindros - Cilindrada:1.598 cm³ - Potência:114 cv @ 5.600 rpm\r\n  (etanol/gasolina) - Torque:15,5 kgfm @ 4.000 rpm (etanol/gasolina) - Taxa de compressão:10,7 +/- 0,2 - Sistema de\r\n  injeção:Digital/multiponto/semissequencial/indireto - Tração:Dianteira</p>\r\n  <h3>FREIOS</h3>\r\n  <p>Tipo:Sistema ABS de 4 canais e 4 sensores com controle eletrônico de distribuição de força (EBD), discos ventilados\r\n  dianteiros e tambores traseiros</p>\r\n</div>\r\n<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>TRANSMISSÃO</h3>\r\n  <p>Tipo:Manual, 5 marchas</p>\r\n  <h3>RODAS/PNEUS</h3>\r\n  <p>Rodas:Aço 16\" - Pneus:205/60 R16</p>\r\n</div>', '<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n  <h3>CÂMBIO</h3>\r\n  <p>Manual de 5 marchas:Item de série</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>CONFORTO E CONVENIÊNCIA</h3>\r\n  <p>Abertura da tampa de combustível por acionamento interno:Item de série - Abertura elétrica das portas e do\r\n  porta-malas:Item de série - Acabamento de tecido nos bancos e nos detalhes das portas:Item de série - Apoios de cabeça\r\n  dianteiros e traseiros separados e ajustáveis para os 5 ocupantes:Item de série - Ar-condicionado:Item de série - Banco\r\n  do motorista com ajustes de altura:Item de série - Banco traseiro bipartido 60/40, dobrável:Item de série - Bancos\r\n  dianteiros com tecnologia Zero Gravity®:Item de série - Controle do computador de bordo no volante:Item de série -\r\n  Direção elétrica com controles de áudio e telefone no volante:Item de série - Para-sol com espelhos para o motorista e\r\n  passageiro:Item de série - Porta-copos no console central e nas portas:Item de série - Porta-malas com iluminação\r\n  interna:Item de série - Porta-objetos na lateral das portas dianteiras e traseiras:Item de série - Porta-revistas nos\r\n  bancos dianteiros:Item de série - Sistema de partida a frio FLEX START®:Item de série - Tomada 12V (2):Item de série -\r\n  Vidros dianteiros e traseiros elétricos com sistema \"one touch down\" somente para o motorista:Item de série - Volante\r\n  com acabamento de poliuretano:Item de série - Volante de três raios com regulagem de altura e profundidade:Item de\r\n  série</p>\r\n</div>\r\n<div class=\'col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\'>\r\n  <h3>APARÊNCIA</h3>\r\n  <p>Aerofólio integrado na cor do veículo:Item de série - Grade frontal com acabamento cromado:Item de série - Maçanetas\r\n  externas na cor do veículo:Item de série - Maçanetas internas de abertura das portas cromadas:Item de série - Rack de\r\n  teto longitudinal na cor prata:Item de série - Retrovisores externos com regulagem elétrica:Item de série -\r\n  Retrovisores externos na cor do veículo:Item de série - Rodas de aço 16\" e pneus 205/60 R16:Item de séries</p>\r\n</div>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `carro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `gallery`
--

INSERT INTO `gallery` (`id`, `url`, `nome`, `carro_id`) VALUES
(1, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Frontier/1.jpg', 'Nissan Frontier', 1),
(2, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Frontier/17.jpg', 'Nissan Frontier', 1),
(3, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-March/1.jpg', 'Nissan March', 2),
(4, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-March/2.jpg', 'Nissan March', 2),
(5, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Kicks/1.jpg', 'Nissan Kicks', 3),
(6, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Kicks/2.jpg', 'Nissan Kicks', 3),
(7, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Kicks/4.jpg', 'Nissan Kicks', 3),
(8, 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/ff8c792c3a067cad4458f9b240fe2f9e/Nissan-Kicks/8.jpg', 'Nissan Kicks', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carro`
--
ALTER TABLE `carro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carro_id` (`carro_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carro`
--
ALTER TABLE `carro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`carro_id`) REFERENCES `carro` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
