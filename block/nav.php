<nav class="navbar navbar-expand-lg navbar-light">
  <a href='/index' class="d-xs-block d-sm-block d-md-block d-lg-none d-xl-none">
    <img alt="Brand" class='brand-logo' 
      src="https://imgcdn0.searchoptics.com/cdno/n/q75/https://blueprint.cdn.cloud.searchoptics.net/69f306048f4f84fd1b955ca2ef42aab8/logo/logo_nissan.png">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mx-auto">
      <li class="nav-item">
        <a class="nav-link text-uppercase font-weight-bold" href="<?php echo URL::getBase(); ?>">Home</a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link text-uppercase font-weight-bold" href="<?php echo URL::getBase(); ?>">Veículos</a>
      </li>-->
      <li class="nav-item">
        <a class="nav-link text-uppercase font-weight-bold" href="#blog">Blog</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-uppercase font-weight-bold" href="<?php echo URL::getBase(); ?>#contato">Contato</a>
      </li>
    </ul>
  </div>
</nav>