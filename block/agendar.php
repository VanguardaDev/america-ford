<div id="bloco" class='py-5 bg-dark text-white'>
  <h1 class='font-weight-bold text-uppercase text-center'>test drive</h1>
  <hr class='pb-2'>
  <div class="row justify-content-center" style="margin-right:0; margin-left:0;">
    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-6 col text-center">
      <p class="px-4">
        Agende um test-drive em uma das lojas de sua preferência, marque o dia e a 
        hora através do nosso formulário e aguarde a confirmação. Caso queira esclarecer 
        alguma dúvida, antes de fazer o agendamento entre em contato conosco através do 
        falo conosco.
      </p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 col-xl-3 col text-center">
      <a href="#" class="btn btn-warning text-uppercase p-4">
        <h1 class='font-weight-bold'>agendar</h1>
      </a>
    </div>
  </div>
</div>