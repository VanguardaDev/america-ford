<?php require("lib/RDStationAPI.class.php"); ?>
<?php

    $rdAPI = new RDStationAPI("8a62a89172594b35b8790017d560ef00", "5ce50b4012e11fd8e451ff7ad852f6ac");
    if(isset($_POST['contatofloat']) && !empty($_POST['contatofloat']))	{
        $send = $rdAPI->sendNewLead($_POST['email'], array(
            "nome" => $_POST['nome'],
            "telefone" => $_POST['telefone'],
            "modelo"=> $_POST['modelo'],
            "identificador" => "website",
            "subscribe_newsletter" => true //custom data is accepted
        ));
        if($send) {
            echo '<script> alert("Sua mensagem foi enviada com sucesso!!"); window.location="https://oferta.americaford.digital/site-old";</script>';
        }
    }
?>
<div class="view altura" style='height: 100%;'>
    <div class=" intro-2">
        <div class="full-bg-img">
            <div class="mask rgba-black-strong">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-sm-12 col-md-6 align-self-center">
                            <h1 class="hero-1 text-white bolder box-txt-mobile">
                                A AMÉRICA FORD TEM VANTAGENS INCRÍVEIS PRA VOCÊ.
                            </h1>
                        </div>
                        <div class="col-12 col-sm-12 col-md-5">
                            <div class="box rounded">
                                <h2 class='h2 text-center text-white font-weight-bold'>
                                    Cadastre-se para conhecer.
                                </h2>
                                <h6 class='text-white'>
                                    Preencha o formulário e conheça todas as facilidades da América Ford para conquistar seu carro novo.
                                </h6>
                                <form method="POST" class='mt-4 text-white'>
                                    <div class="form-group">
                                        <label for="nome">Nome*</label>
                                        <input type="text" class="form-control" id="nome" name="nome" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="telefono">Telefone*</label>
                                        <input type="tel" class="form-control" id="telefone" name="telefone" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email*</label>
                                        <input type="email" class="form-control" id="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="modelo">Modelo do Carro*</label>
                                        <select class="custom-select" name='modelo' id='modelo'>
                                            <option selected>Selecione carro</option>
                                            <option value="Ford Ka">Ford Ka</option>
                                            <option value="Ford Ranger">Ford Ranger</option>
                                            <option value="Ford Eco">Ford Eco</option>
                                        </select>
                                    </div>
                                    <input name="contatofloat" type="hidden" id="contatofloat" value="1">
                                    <div class="pt-3">
                                        <button type="submit" class="mt-1 btn btn-block btn-outline-secondary text-white">
                                            CADASTRAR
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
