<div class="row justify-content-center">
  <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4 py-2">
    <a href="#">
      <img src="https://imgcdn0.searchoptics.com/cdno/n/q75/https://blueprint.cdn.cloud.searchoptics.net/69f306048f4f84fd1b955ca2ef42aab8/Home/box_1.jpg" 
        alt="" title="" class='img-fluid w-100'>
    </a>
    <a href="#" class='btn btn-lg btn-block btn-materiais'>
      <h5 class='text-white font-weight-bold'>Text 1</h5>
    </a>
  </div>
  <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4 py-2">
    <a href="#">
      <img src="https://imgcdn0.searchoptics.com/cdno/n/q75/https://blueprint.cdn.cloud.searchoptics.net/69f306048f4f84fd1b955ca2ef42aab8/Home/box_2.jpg" 
        alt="" title="" class='img-fluid w-100'>
    </a>
    <a href="#" class='btn btn-lg btn-block btn-materiais'>
      <h5 class='text-white font-weight-bold'>Text 2</h5>
    </a>
  </div>
  <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4 py-2">
    <a href="#">
      <img src="https://imgcdn0.searchoptics.com/cdno/n/q75/https://blueprint.cdn.cloud.searchoptics.net/69f306048f4f84fd1b955ca2ef42aab8/Home/box_3.jpg" 
        alt="" title="" class='img-fluid w-100'>
    </a>
    <a href="#" class='btn btn-lg btn-block btn-materiais'>
      <h5 class='text-white font-weight-bold'>Text 3</h5>
    </a>
  </div>
</div>