<div class="row text-center">
	<div class="col-md-4 col-lg-4 py-5">
		<img class='img-fluid' src="<?php echo URL::getBase(); ?>assets/img/ka.png" alt="">
		<h5 class='text-danger font-weight-bold text-uppercase pt-2'>novo ka</h5>
		<p class='p-08'>Um carro que reúne design,<br> espaço interno e tecnologia inteligente.<br> Um carro feito pra você.</p>
		<a href="<?php echo URL::getBase(); ?>detalhe/ford-ka" class='btn btn-outline-danger'>Ver Detalhes</a>
	</div>
	<div class="col-md-4 col-lg-4 py-5">
		<img class='img-fluid' src="<?php echo URL::getBase(); ?>assets/img/ranger.png" alt="Ranger">
		<h5 class='text-danger font-weight-bold text-uppercase pt-3'>Ranger</h5>
		<p class='p-08'>A Ranger te acompanha no sol,<br> na chuva, ou nos dois. O desempenho que você precisa<br> para viver altas aventuras.</p>
		<a href="<?php echo URL::getBase(); ?>detalhe/ranger" class='btn btn-outline-danger'>Ver Detalhes</a>
	</div>
	<div class="col-md-4 col-lg-4 py-5">
		<img class='img-fluid' src="<?php echo URL::getBase(); ?>assets/img/eco.png" alt="Eco Sport">
		<h5 class='text-danger font-weight-bold text-uppercase pt-3'>eco</h5>
		<p class='p-08'>Ir mais além e com muita economia,<br> só com o Ford Ecosport.<br> Perfeito pra que deseja ultrapassar barreiras.</p>
		<a href="<?php echo URL::getBase(); ?>detalhe/eco" class='btn btn-outline-danger'>Ver Detalhes</a>
	</div>
</div>