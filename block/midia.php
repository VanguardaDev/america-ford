<!-- <hr class='hr-footer d-none d-lg-block'>
<ul class="nav justify-content-center pt-5">
  <li class="nav-item">
    <a class="nav-link text-white" href="#">
      <i class="fab fa-facebook-f fa-3x"></i>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-white" href="#">
      <i class="fab fa-instagram fa-3x"></i>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-white" href="#">
      <i class="fab fa-youtube fa-3x"></i>
    </a>
  </li>
</ul> -->
<p class="pt-3 pb-4 text-center mb-0">
  <small>
    ©2018  Todos os Direitos Reservados.
    <a href='https:/vanguardacomunicacao.com.br' target='_blank' class='text-white font-weight-bold'>Vanguarda Comunicação</a>
  </small>
</p>
<a href='#top' class='btn btn-block btn-outline-warning'>
  <span class="">
    VOLTAR AO INÍCIO
    <i class="fas fa-chevron-circle-up ml-3"></i>
  </span>
</a