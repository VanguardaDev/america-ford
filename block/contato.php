<div class="row no-gutters h-100 bg-america" id='contato'>
  <div class="col-md-6 col-xs-12 cl-sm-12 col-lg-6 align-self-center py-mobile">
    <div class="text-center pt-4">
      <p class='text-white bolder'><i class="far fa-clock fa-4x"></i></p>
      <p class='text-white bolder mb-1'>Segunda a Sexta - 08h às 18h</p>
      <p class='text-white bolder'>Sábado - 08h às 16h</p>
    </div>
    <div class="text-center pt-4">
      <p class='text-white bolder'><i class="fas fa-headset fa-4x"></i></p>
      <p class='text-white bolder mb-1'><i class="fas fa-phone mr-2"></i>(92) 98445.2799</p>
      <p class='text-white bolder'><i class="fab fa-whatsapp mr-2"></i>(92) 98445.2799</p>
    </div>
    <div class="text-center pt4">
    <a class='text-ford-2 mr-3' TARGET='_blank' href="">
        <i class="fab fa-instagram fa-3x"></i></a>
      <a class='text-ford-2' TARGET='_blank' href="">
        <i class="fab fa-facebook fa-3x"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-xs-12 cl-sm-12 col-lg-6 d-none d-md-block d-lg-block d-xl-block">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1992.0123526055788!2d-59.9890867!3d-3.0880794!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x926c0fb313f6e111%3A0x934b1da9be426657!2sAmerica+Ford+-+S%C3%A3o+Jorge!5e0!3m2!1spt-BR!2sbr!4v1548277100296" 
        width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>